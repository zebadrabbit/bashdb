<?php
	include 'config.php';
	require_once('recaptchalib.php'); // reCAPTCHA Library
	$pubkey = ""; // Public API Key - move to config.php
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>bqDB - Brony Quote Database</title>
<link href="main.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="container">

	<?php include 'header.php'; ?>

    <div id="leftcontainerextralarge">
		<form id="form1" name="form1" method="post" action="inc/submit.php">
            <p>
              <textarea name="quote" id="quote" cols="70" rows="10"></textarea>
            </p>
	    <p>
		<?php
			echo recaptcha_get_html($pubkey); // Display reCAPTCHA
		?>
	    </p>
            <p>
              <input type="submit" name="Submit" id="Submit" value="Submit" />
            </p>
            <p><strong>Acceptable Posting Policy:</strong><br />
              o Please stick to posting content from MLP-related chatrooms, forums and IRC.<br />
              o Please remove timestamps before submitting (unless the timestamp is relevant to the conversation).<br />
              o Do not post content of a sexual nature (discussions about shipping are okay).<br />
              o Do not post conversations of a personal or private nature.<br />
              o Do not post content containing sexual, violent, racist, inflammatory or otherwise private roleplay.<br />
            o The moderators reserve the right to accept or reject content at their discretion.</p>
        </form>
    </div>
    <div id="horizdivider"></div>
	
	<?php include 'inc/footer.php'; ?>

</div>

</body>
</html>
