<?php

	session_start();
	if (is_null($_SESSION['uid'])) {
		header("location:../index.php");	
	} else {
		include '../config.php';
	
		$link = mysql_connect($db_host, $db_user, $db_pass);
		if (!$link) {
			die('Could not connect: ' . mysql_error());
		}
		
		$db_selected = mysql_select_db($db_name, $link);
		if (!$db_selected) {
			die ('Can\'t use: ' . mysql_error());
		}

		$motd = htmlspecialchars($_POST['motd'], ENT_COMPAT);
		$motd = mysql_real_escape_string($motd);

		mysql_query("insert into motd (created,motd) values (" . time() . ",'" . $motd . "')");
		mysql_close($link);
	
		header("location:main.php");
	}

?>