<?php

	session_start();
	if (is_null($_SESSION['uid'])) {
		header("location:../index.php");	
	} else {

		include '../config.php';

		$file = file_get_contents('notices_template.php', true);

		$link = mysql_connect($db_host, $db_user, $db_pass);
		if (!$link) {
			die('Could not connect: ' . mysql_error());
		}
		
		$db_selected = mysql_select_db($db_name, $link);
		if (!$db_selected) {
			die ('Can\'t use: ' . mysql_error());
		}

		$result = mysql_query("select created,notice from notices order by created desc");
	
		while ($row = mysql_fetch_row($result)) {
			$tmp = $file;
			$tmp = str_replace('$CREATED$', date('m-d-Y', intval($row[0])), $tmp);
			$tmp = str_replace('$NOTICE$', nl2br($row[1]), $tmp);
			
			echo $tmp;
		}	

	}
?>