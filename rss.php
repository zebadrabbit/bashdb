<?php
	header("Content-type: application/xml"); 
	echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
?>

<feed xml:lang="en-US" xmlns="http://www.w3.org/2005/Atom">

	<id>http://bronydb.com/rss.php</id>
	
	<title>BronyDB RSS Syndication</title>
	<updated>2007-01-01T00:00:02Z</updated>
	<link rel="self" href="http://bronydb.com/rss.php" type="application/atom+xml" />
	
	<author>
		<name>BronyDB</name>
		<uri>http://bronydb.com</uri>
		<email>octavia@bronydb.com</email>
	</author>  

<?php

	include 'config.php';
	$link = mysql_connect($db_host, $db_user, $db_pass);
	
	if (!$link) {
	    die("Could not connect: " . mysql_error());
	}
	
	$db_selected = mysql_select_db($db_name, $link);
	if (!$db_selected) {
		die ("Can't use: " . mysql_error());
	}

	$result = mysql_query("select * from quotes where status=2 or status=3 order by created desc limit 20");

	$regex = '/[\x00-\x1f]/';

	while ($row = mysql_fetch_row($result)) {

		echo "<entry>\r\n";
		echo "	<title>Quote " . $row[0] . "</title>\r\n";
		echo "	<category term='Quote'/>\r\n";
		echo "	<id>http://bronydb.com/" . $row[0] . "</id>\r\n";
		echo "	<published>2007-01-01T00:00:00Z</published>\r\n";
		echo "	<updated>2007-01-01T00:00:00Z</updated>\r\n";
		echo "	<link href='" . $row[0] . "'/>\r\n";
		echo "	<summary>" . preg_replace($regex, '', $row[2]) . "</summary>\r\n";
		echo "	<content>" . preg_replace($regex, '', $row[2]) . "</content>\r\n";
		echo "</entry>\r\n\r\n";

	}
	
	mysql_close($link);
		
?>

</feed>
